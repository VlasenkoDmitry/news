import Foundation

class FilterData {
    
    var find: String?
    var country: String?
    var category: String?
    var source: String?
    
    init (find: String?, country: String?,category: String?, source: String?) {
        self.find = find
        self.country = country
        self.category = category
        self.source = source
    }
}
