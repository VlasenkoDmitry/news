import Foundation

class Requests {
    
    private var key = "f831d8a3380540d58e0c4d07c89e4d56"
    private var pageSize = 5
    
    func downloadImage(link: String) -> Data? {
        var newLink = link
        if newLink == ""{
            newLink = "https://icon-library.com/images/no-image-icon/no-image-icon-26.jpg"
        }
        let url = URL(string: newLink)
        let data = try? Data(contentsOf: url!)
        return data
    }
    
    private func linkFormation(filter: FilterData,pageNumber: Int) -> String {
        let from = Date().formatSmall()
        let numPage = pageNumber
        var counter = 0
        var link = "https://newsapi.org/v2/top-headlines?" + "apiKey="+"\(key)"
        if let find = filter.find {
            link += "&q=" + "\(find)"
            counter += 1
        }
        if let source = filter.source {
            link += "&sources=" + "\(source)"
            counter += 1
        }
        if let country = filter.country {
            link += "&country=" + "\(country)"
            counter += 1
        }
        if let category = filter.category {
            link += "&category=" + "\(category)"
            counter += 1
        }
        if counter == 0 {
            link += "&sources=bbc-news"
        }
        link += "&from=" + "\(from)" + "&page=" + "\(numPage)" + "&pageSize="+"\(pageSize)"
        return link
    }
    
    private func parsingJSON(articles: [[String : Any]], dataNews: inout [DataCellTable]){
        for article in articles {
            let news = DataCellTable(image: nil, imageLink: "", author: "", title: "", descript: "", link: "", source: "")
            if let imageLink = article["urlToImage"] as? String {
                news.imageLink = imageLink
            }
            if let author = article["author"] as? String {
                news.author = author
            }
            if let title = article["title"] as? String {
                news.title = title
            }
            if let description = article["description"] as? String {
                news.descript = description
            }
            if let url = article["url"] as? String {
                news.link = url
            }
            if let source = article["source"] as? [String: Any] {
                if let name = source["name"] as? String {
                    news.source = name
                }
            }
            dataNews.append(news)
        }
    }
    
    func sendRequest(data: [DataCellTable],filter: FilterData,totalResultLastRequest: Int,numberOfCells: Int, complition: @escaping ([DataCellTable]?,Int)->()) {
        var dataNews = data
        var totalResultRequest = Int()
        let pageNumber = numberOfCells / pageSize + 1
        guard let url = URL(string: linkFormation(filter: filter,pageNumber: pageNumber)) else { return }
        print(url)
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if data == nil {}
            if error == nil, let data = data {
                do {
                    if let json = try JSONSerialization.jsonObject(with: data) as? [String : Any] {
                        if let totalResults = json["totalResults"] as? Int {
                            totalResultRequest = totalResults
                        }
                        if let articles = json["articles"] as? [[String:Any]] {
                            self.parsingJSON(articles: articles, dataNews: &dataNews)
                            complition(dataNews,totalResultRequest)
                        }
                    }
                } catch {
                    print(error)
                }
            }
        }
        task.resume()
    }
}
