import Foundation
import RealmSwift

class NewsRealmData: Object {
    
    @objc dynamic var image: Data? = nil
    @objc dynamic var imageLink: String = ""
    @objc dynamic var author: String = ""
    @objc dynamic var title: String = ""
    @objc dynamic var descript: String = ""
    @objc dynamic var link: String = ""
    @objc dynamic var source: String = ""
}
