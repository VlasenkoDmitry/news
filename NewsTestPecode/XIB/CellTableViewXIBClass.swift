import UIKit

class CellTableViewXIBClass: UIView {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var descript: UILabel!
    @IBOutlet weak var source: UILabel!
    
    static func instanceFromNib(cell: DataCellTable ) -> CellTableViewXIBClass {
        guard let xib = UINib(nibName: "CellTableViewXIBClass", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CellTableViewXIBClass else { return CellTableViewXIBClass()}
        let requestImage = Requests()
        DispatchQueue.global(qos: .userInitiated).async {
            if let image = requestImage.downloadImage(link: cell.imageLink) {
                cell.image = image
                DispatchQueue.main.sync {
                    if let imageData = cell.image {
                        xib.image.image = UIImage(data: imageData)
                    }
                }
            }
        }
        xib.layer.cornerRadius = 15
        xib.image.contentMode = .scaleAspectFill
        xib.image.layer.cornerRadius = 15
        xib.author.text = cell.author
        xib.title.text = cell.title
        xib.descript.text = cell.descript
        xib.source.text = cell.source
        return xib
    }
    
}
