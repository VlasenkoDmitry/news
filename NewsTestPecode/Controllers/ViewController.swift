import UIKit
import WebKit
import RealmSwift

class ViewController: UIViewController {
    
    @IBOutlet weak var filtersButton: UIButton!
    @IBOutlet weak var reloadButton: UIButton!
    @IBOutlet weak var favouritesButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searshBar: UISearchBar!
    
    var dataNews = [DataCellTable]()
    var basicSourse = "bbc-news"
    var filters = FilterData(find: nil, country: nil, category: nil, source: nil)
    var webView = WKWebView()
    var backButton = UIButton()
    var realm = try! Realm()
    var totalResultLastRequest = 0
    let refreshControll: UIRefreshControl = {
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(refresh(sender:)), for: .valueChanged)
        return refresh
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filters.source = basicSourse
        realmFile()
        tableView.refreshControl = refreshControll
        reloadData()
    }
    
    func reloadData() {
        let request = Requests()
        dataNews = [DataCellTable]()
        filters.find = searshBar.text?.lowercased()
        if filters.country == nil && filters.category == nil && filters.source == nil && filters.find == nil {
            filters.source = basicSourse
        }
        totalResultLastRequest = 1
        request.sendRequest(data: dataNews, filter: filters,totalResultLastRequest: totalResultLastRequest, numberOfCells: 0) { dataNews, totalResult in
            self.totalResultLastRequest = totalResult
            guard let data = dataNews else { return }
            self.dataNews = data
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    private func downloadMoreNews(numberOfCells: Int){
        let request = Requests()
        let numberCellBeforeRequest = dataNews.count
        filters.find = searshBar.text?.lowercased()
        request.sendRequest(data: dataNews, filter: filters, totalResultLastRequest: totalResultLastRequest,numberOfCells: numberOfCells) { dataNews, totalResult in
            self.totalResultLastRequest = totalResult
            guard let data = dataNews else { return }
            self.dataNews = data
            DispatchQueue.main.async {
                self.addNewCells(countCells: numberCellBeforeRequest)
            }
        }
    }
    
    private func addNewCells(countCells: Int) {
        tableView.beginUpdates()
        for i in countCells..<dataNews.count {
            self.tableView.insertRows(at: [IndexPath.init(row: i - 1, section: 0)], with: .automatic)
        }
        tableView.endUpdates()
    }
    
    private func realmFile() {
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        var configuration = Realm.Configuration()
        configuration.deleteRealmIfMigrationNeeded = true
        realm = try! Realm(configuration: configuration)
    }
    
    private func checkingAvailabilityInRealm(cell: NewTableViewCell) {
        do {
            try realm.write({
                if realm.objects(NewsRealmData.self).filter("imageLink = %@", dataNews[cell.heartButton.tag].imageLink).first != nil {
                    cell.heartButton.isSelected = true
                } else {
                    cell.heartButton.isSelected = false
                }
            })
        } catch let error {
            print(error)
        }
    }
    
    @IBAction private func filtersPressed(_ sender: UIButton) {
        guard let controler = self.storyboard?.instantiateViewController(identifier: "FiltersViewController") as? FiltersViewController else { return }
        controler.delegate = self
        controler.filter = filters
        self.navigationController?.pushViewController(controler, animated: true)
    }
    
    @IBAction private func favouritePressed(_ sender: UIButton) {
        guard let controler = self.storyboard?.instantiateViewController(identifier: "FavouritesViewController") as? FavouritesViewController else { return }
        controler.news = readAllfavouritesNews()
        self.navigationController?.pushViewController(controler, animated: false)
    }
    
    private func readAllfavouritesNews () -> [String] {
        var listFavourites = [String]()
        do {
            try realm.write({
                let result = realm.objects(NewsRealmData.self).count
                for news in 0..<result {
                    listFavourites.append(realm.objects(NewsRealmData.self)[news].title)
                }
            })
        } catch let error {
            print(error)
        }
        return listFavourites
    }
    
    @objc private func refresh(sender: UIRefreshControl) {
        reloadData()
        sender.endRefreshing()
    }
    
    @IBAction private func reloadPressed(_ sender: UIButton) {
        reloadData()
    }
    
    private func webOpen(link: String) {
        webView = WKWebView(frame: view.frame)
        webView.frame.origin.y += 100
        webView.frame.size.height -= 100
        view.addSubview(webView)
        if let url = addUrl(link: link) {
            webView.load(url)
        }
        addButtonBack()
        hideButtons()
        backButton.addTarget(self, action: #selector(backPressed), for: .touchUpInside)
    }
    
    private func addUrl(link: String) -> URLRequest? {
        guard let url = URL(string: link) else { return nil}
        return URLRequest(url: url)
    }
    
    private func addButtonBack() {
        backButton.setTitle("Back", for: .normal)
        backButton.setTitleColor(filtersButton.tintColor, for: .normal)
        backButton.frame = CGRect(x: 20, y: filtersButton.frame.origin.y, width: 50, height: 50)
        view.addSubview(backButton)
    }
    
    private func hideButtons() {
        filtersButton.isHidden = true
        reloadButton.isHidden = true
        favouritesButton.isHidden = true
    }
    
    @objc private func backPressed() {
        filtersButton.isHidden = !filtersButton.isHidden
        reloadButton.isHidden = !reloadButton.isHidden
        favouritesButton.isHidden = !favouritesButton.isHidden
        backButton.removeFromSuperview()
        webView.removeFromSuperview()
    }
}

extension ViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataNews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewTableViewCell", for: indexPath) as? NewTableViewCell else { return UITableViewCell() }
        if self.dataNews.count == 0 { return UITableViewCell() }
        cell.configure(dataNews: self.dataNews[indexPath.row], index: indexPath.row)
        cell.delegate = self
        checkingAvailabilityInRealm(cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        webOpen(link: dataNews[indexPath.row].link)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if totalResultLastRequest > dataNews.count {
            if indexPath.row + 1  == dataNews.count  {
                downloadMoreNews(numberOfCells: indexPath.row + 1)
            }
        }
    }
}

extension ViewController: FiltersViewControllerDelegate {
    func filtersBack(country: String?, category: String?, source: String?) {
        filters.country = country
        filters.category = category
        filters.source = source
    }
}

extension ViewController: NewTableViewCellDelegate {
    func addNewsInFavourite(index: Int) {
        do {
            try realm.write({
                realm.add(objectRealm(index: index))
            })
        } catch let error {
            print(error)
        }
    }
    
    func deleteNewsInFavourite(index: Int) {
        do {
            try realm.write({
                guard let item = realm.objects(NewsRealmData.self).filter("imageLink = %@", dataNews[index].imageLink).first else {return}
                realm.delete(item)
            })
        } catch let error {
            print(error)
        }
    }
    
    private func objectRealm(index: Int) -> NewsRealmData{
        let object = NewsRealmData()
        object.link = dataNews[index].link
        object.image = dataNews[index].image
        object.imageLink = dataNews[index].imageLink
        object.descript = dataNews[index].descript
        object.author = dataNews[index].author
        object.title = dataNews[index].title
        object.source = dataNews[index].source
        return object
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        reloadData()
    }
}
