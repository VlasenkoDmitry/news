protocol FiltersViewControllerDelegate: AnyObject {
    func filtersBack(country: String?,category: String?,source: String?)
}

import UIKit

class FiltersViewController: UIViewController {
    
    @IBOutlet weak var courtryPicker: UIPickerView!
    @IBOutlet weak var categoryPicker: UIPickerView!
    @IBOutlet weak var source: UITextField!
    
    weak var delegate: FiltersViewControllerDelegate?
    
    var filter = FilterData(find: nil, country: nil, category: nil, source: nil)
    var arrayCountry = [String]()
    var arrayCategory = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        source.text = filter.source
        
        arrayCountry = ["","ae","ar","at","au","be","bg","br","ca","ch","cn","co","cu","cz","de","eg","fr","gb","gr","hk","hu","id","ie","il","in","it","jp","kr","lt","lv","ma","mx","my","ng","nl","no","nz","ph","pl","pt","ro","rs","ru","sa","se","sg","si","sk","th","tr","tw","ua","us","ve","za"]
        arrayCategory = ["","business","entertainment","general","health","science","sports","technology"]
        
        if let country = filter.country {
            courtryPicker.selectRow(arrayCountry.firstIndex(of: country)!, inComponent: 0, animated: false)
        }
        if let category = filter.category {
            categoryPicker.selectRow(arrayCategory.firstIndex(of: category)!, inComponent: 0, animated: false)
        }
    }
    
    @IBAction func backPessed(_ sender: UIButton) {
        delegate?.filtersBack(country: filter.country, category: filter.category, source: source.text)
        navigationController?.popViewController(animated: true)
    }
}

extension FiltersViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 1 {
            return arrayCountry.count
        } else {
            return arrayCategory.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 1 {
            return arrayCountry[row]
        } else {
            return arrayCategory[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 1 {
            filter.country = arrayCountry[row]
        } else {
            filter.category = arrayCategory[row]
        }
    }
}
