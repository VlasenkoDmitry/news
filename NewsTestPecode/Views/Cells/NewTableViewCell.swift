protocol NewTableViewCellDelegate: AnyObject {
    func addNewsInFavourite(index: Int)
    func deleteNewsInFavourite(index: Int)
}

import UIKit

class NewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var heartButton: UIButton!
    
    weak var delegate: NewTableViewCellDelegate?
    
    func configure(dataNews: DataCellTable,index: Int) {
        let customView = CellTableViewXIBClass.instanceFromNib(cell: dataNews)
        customView.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: 250)
        heartButton.tag = index
        self.addSubview(customView)
    }
    
    @IBAction func heartPressed(_ sender: UIButton) {
        heartButton.isSelected = !heartButton.isSelected
        if heartButton.isSelected{
            delegate?.addNewsInFavourite(index: sender.tag)
        } else {
            delegate?.deleteNewsInFavourite(index: sender.tag)
        }
    }
}
