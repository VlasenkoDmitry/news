import UIKit

class FavouritesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    
    func configure(title: String) {
        self.title.text = title
    }
}
